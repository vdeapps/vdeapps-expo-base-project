# Installation du projet

## Requires

`mkdir /opt/devels`

### Nodejs
mkdir /opt/apps && cd /opt/apps
wget https://nodejs.org/dist/v16.15.1/node-v16.15.1-linux-x64.tar.xz

Décompresser dans le dossier node-v16
Rendre disponible les binaries : 
```shell
cd /usr/local/bin
sudo ln -s /opt/apps/node-v16/bin/* .
```

### yarn
`sudo corepack enable`

### Expo / eas
```shell
sudo npm install --location=global expo-cli eas-cli
cd /usr/local/bin && sudo ln -s /opt/apps/node/bin/* .
```

## Nouveau projet

### Init
- `cd /opt/devels`
- `expo init <projName> && cd <projName>`, Choisir le projet par défaut `minimal`

### Config app.json
Ajouter au fichier app.json
- `"owner": "vdeapps",`
- Pour un projet existant, modifier le nom "name" et "slug"

### Config eas
- Se logger : `eas login`
- Sélectionner le type de projet : `eas build:configure`

- Code depend on environment variables ? [Add them to your build configuration](https://docs.expo.dev/build-reference/variables).
- Do you use private npm packages? [Add your npm token](https://docs.expo.dev/build-reference/private-npm-packages/).

## Run build

### Android
- by Expo : `eas build --platform android`
- local : `eas build --platform android --local`
### IOS
`eas build --platform ios`

## Deploy

- [Distribute your app to an app store](https://docs.expo.dev/build/setup/#distribute-your-app-to-an-app-store)